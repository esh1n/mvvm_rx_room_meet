package com.esh1n.core_android.ui.viewmodel

import com.esh1n.core_android.ui.viewmodel.Resource.Status.SUCCESS

data class Resource<T>(val status: Status, val data: T?, val message: String?) {


    enum class Status {
        SUCCESS, ERROR, LOADING, ENDED
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null)
        }

        fun <T, R> success(data: Resource<R>, func: (R?)->T): Resource<T> {
            return Resource(SUCCESS, func.invoke(data.data), data.message)
        }

        fun <T> error(msg: String): Resource<T> {
            return Resource(
                Status.ERROR,
                null,
                msg
            )
        }

        fun <T> loading(): Resource<T> {
            return Resource(
                Status.LOADING,
                null,
                null
            )
        }

        fun <T> ended(): Resource<T> {
            return Resource(
                Status.ENDED,
                null,
                null
            )
        }
    }
}
