package com.dataart.dameet.presentation.di.module

import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class DataModule : BaseDataModule() {

//    @Provides
//    @Singleton
//    @Named("no_session")
//    fun provideOkHttpClient(cache: CacheContext, rxBus: RxJobResultBus): OkHttpClient {
//        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
//            .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
//            .withReadTimeOut(READ_WRITE_TIMEOUT)
//            .withWriteTimeOut(READ_WRITE_TIMEOUT)
//            .withConnectTimeOut(CONNECT_TIMEOUT)
//            .withSelfSignedSSLSocketFactory()
//            .build()
//    }
//
//    @Provides
//    @Singleton
//    fun provideAuthorizationApiService(@Named("no_session") client: OkHttpClient): AuthorizationApiService {
//        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
//            .withOkHttpClient(client)
//            .withConverterFactory(GsonConverterFactory.create())
//            .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
//            .build(AuthorizationApiService::class.java)
//    }
//
//
//    @Provides
//    @Singleton
//    @Named("session")
//    fun provideUserSessionOkHttpClient(
//        authorizationApiService: AuthorizationApiService,
//        cache: CacheContext,
//        rxBus: RxJobResultBus
//    ): OkHttpClient {
//        val tokenAuthenticator = TokenAuthenticator(authorizationApiService, cache)
//        return OkHttpBuilder.newBuilder(BuildConfig.API_ENDPOINT)
//            .withLoggingInterceptor(HttpLoggingInterceptor.Level.BODY)
//            .withReadTimeOut(READ_WRITE_TIMEOUT)
//            .withWriteTimeOut(READ_WRITE_TIMEOUT)
//            .withConnectTimeOut(CONNECT_TIMEOUT)
//            .withAuthorizationHeaderInterceptor(cache)
//            //.withUnauthorizedErrorInterceptor(rxBus)
//            .withUnauthorizedErrorInterceptor()
//            .withSelfSignedSSLSocketFactory()
//            .withAuthenticator(tokenAuthenticator)
//            .build()
//    }
//
//
//    @Provides
//    @Singleton
//    fun provideUserSessionApiService(@Named("session") client: OkHttpClient): UserSessionApiService {
//        return ApiServiceBuilder.newBuilder(BuildConfig.API_ENDPOINT)
//            .withOkHttpClient(client)
//            .withConverterFactory(GsonConverterFactory.create())
//            .withCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
//            .build(UserSessionApiService::class.java)
//    }

    companion object {

        private val READ_WRITE_TIMEOUT = 60
        private val CONNECT_TIMEOUT = 5
    }

}