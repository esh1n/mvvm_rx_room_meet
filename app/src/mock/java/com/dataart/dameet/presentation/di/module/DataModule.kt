package com.dataart.dameet.presentation.di.module

import com.dataart.dameet.data.network.response.TokenResponse
import com.dataart.dameet.data.network.response.UserResponse
import com.esh1n.utils_android.TestUtils
import com.dataart.dameet.data.network.AuthorizationApiService
import com.dataart.dartcard.data.UserSessionApiService
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.Module
import dagger.Provides
import io.reactivex.Single
import java.lang.Exception
import javax.inject.Singleton

@Module
class DataModule {

   private val userSessionApiService = object:UserSessionApiService {
       private val gson = Gson()
       override val users: Single<List<UserResponse>>
           get()  {
               val mockUsers: List<UserResponse>
               mockUsers = try{
                   val json = TestUtils.loadResourceAsString("/users.json");
                   val arrayType = object : TypeToken<List<UserResponse>>() {}.type
                   gson.fromJson(json,arrayType)
               }catch (e:Exception){
                   emptyList()
               }
               return Single.just(mockUsers)
           }

   }

    private val authorizationApiService = object : AuthorizationApiService {
        override fun getToken(login: String,password:String): Single<TokenResponse> {
            return Single.just(TokenResponse(login))
        }

    }

    @Provides
    @Singleton
    fun provideAuthorizationApiService(): AuthorizationApiService {
        return authorizationApiService
    }

    @Provides
    @Singleton
    fun provideUserSessionApiService(): UserSessionApiService {
        return userSessionApiService
    }
}