package com.dataart.dameet.presentation.main

import android.arch.lifecycle.MutableLiveData
import com.esh1n.core_android.rx.SchedulersFacade
import com.dataart.dameet.domain.main.UserRepository
import com.esh1n.core_android.ui.viewmodel.Resource
import com.dataart.dameet.presentation.main.mapper.UserMapper
import com.dataart.dameet.presentation.main.model.UserModel
import com.esh1n.core_android.ui.viewmodel.BaseViewModel


class MainViewModel internal constructor(private val usersRepository: UserRepository) : BaseViewModel() {

    val users = MutableLiveData<Resource<List<UserModel>>>()
    get() {
        //loadUsers()
        return field
    }
    private val userMapper = UserMapper()
    fun loadUsers() {
        users.postValue(Resource.loading())
        addDisposable(
            usersRepository.loadUsers()
                .map { userMapper.map(it) }
                .compose(SchedulersFacade.applySchedulersObservable())
                .doOnError() { throwable -> users.postValue(Resource.error(throwable.message ?: "")) }
                .subscribe { models -> users.postValue(Resource.success(models)) })
    }
}