package com.dataart.dameet.presentation.main.mapper

import com.esh1n.utils_android.Mapper
import com.dataart.dameet.data.local.room.entity.UserEntry
import com.dataart.dameet.presentation.main.model.UserModel

class UserMapper: Mapper<UserEntry, UserModel>() {
    override fun map(source: UserEntry): UserModel {
        return UserModel(id = source.id,name = source.name,owner = source.owner)
    }
}