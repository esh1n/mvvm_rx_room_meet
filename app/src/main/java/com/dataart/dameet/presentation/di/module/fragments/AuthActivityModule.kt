package com.dataart.dartcard.presentation.di.fragments


import com.dataart.dameet.presentation.auth.AuthFragment
import com.dataart.dameet.presentation.di.module.ViewModelsModule

import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
interface AuthActivityModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeAuthFragment(): AuthFragment
}

