package com.dataart.dameet.presentation.main

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.dataart.dameet.R
import com.esh1n.core_android.ui.activity.SingleFragmentActivity
import com.esh1n.core_android.ui.startActivity

class MainActivity : SingleFragmentActivity() {

    override fun getStartScreen(savedInstanceState: Bundle?) = MainFragment()

    companion object {

        fun start(context: Context) {
            context.startActivity<MainActivity>()
        }
    }
}
