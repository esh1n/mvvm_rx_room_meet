package com.dataart.dameet.presentation.auth

import android.app.Activity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.esh1n.core_android.ui.activity.SingleFragmentActivity
import com.esh1n.core_android.ui.startActivity

class AuthActivity : SingleFragmentActivity() {

    override fun getStartScreen(savedInstanceState: Bundle?): Fragment {
        return AuthFragment()
    }

    companion object {


        fun start(activity: Activity?) {
            activity.startActivity<AuthActivity>()
        }
    }
}