package com.dataart.dameet.presentation.auth.viewmodel

import android.arch.lifecycle.MutableLiveData
import com.esh1n.core_android.rx.SchedulersFacade
import com.dataart.dameet.domain.auth.AuthRepository
import com.esh1n.core_android.ui.viewmodel.Resource
import com.esh1n.core_android.ui.viewmodel.BaseViewModel
import com.esh1n.core_android.ui.viewmodel.SingleLiveEvent

class AuthViewModel(private val repository: AuthRepository) : BaseViewModel() {

    val onLoggedInEvent = MutableLiveData<Resource<Unit>>()

    val onLoggedOutEvent = SingleLiveEvent<Void>()

    fun login(login: String, password: String) {
        onLoggedInEvent.postValue(Resource.loading())
        addDisposable(repository.login(login, password)
            .compose(SchedulersFacade.applySchedulersCompletable())
            .doOnError() { error ->  onLoggedInEvent.postValue(Resource.error(error.message?:"")) }
            .subscribe { onLoggedInEvent.postValue(Resource.success(Unit)) })

    }

    fun logout() {
        addDisposable(
            repository.logout()
                .compose(SchedulersFacade.applySchedulersCompletable())
                .subscribe { onLoggedOutEvent.call() }
        )

    }
}
