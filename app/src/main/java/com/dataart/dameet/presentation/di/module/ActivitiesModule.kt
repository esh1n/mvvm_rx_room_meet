package com.dataart.dameet.presentation.di.module

import com.dataart.dameet.presentation.auth.AuthActivity
import com.dataart.dameet.presentation.main.MainActivity
import com.dataart.dameet.presentation.start.StartActivity
import com.dataart.dartcard.presentation.di.fragments.AuthActivityModule
import com.dataart.dartcard.presentation.di.fragments.CardsActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivitiesModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class, CardsActivityModule::class])
    fun contributeCardsActivity(): MainActivity

    @ContributesAndroidInjector(modules = [AuthActivityModule::class])
    fun contributeAuthActivity(): AuthActivity

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun contributeStartActivity(): StartActivity
}
