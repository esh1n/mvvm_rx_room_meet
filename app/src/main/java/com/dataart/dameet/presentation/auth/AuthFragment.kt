package com.dataart.dameet.presentation.auth

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.View
import com.dataart.dameet.R
import com.dataart.dameet.databinding.FragmentAuthBinding
import com.dataart.dameet.presentation.auth.viewmodel.AuthViewModel
import com.dataart.dameet.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dameet.presentation.main.MainActivity
import com.esh1n.core_android.ui.fragment.BaseVMFragment
import com.esh1n.core_android.ui.viewmodel.Resource
import com.esh1n.utils_android.SnackbarBuilder
import javax.inject.Inject

class AuthFragment : BaseVMFragment<AuthViewModel>() {

    @Inject
    override lateinit var factory: AuthViewModelFactory

    override val viewModelClass = AuthViewModel::class.java

    override val layoutResource = R.layout.fragment_auth

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeLogInEvent()
    }

    private var binding: FragmentAuthBinding? = null

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        binding?.let {

            it.emailSignInButton.setOnClickListener { _ ->
                val login = it.email.text.toString()
                val pass = it.password.text.toString()
                viewModel.login(login,pass)
            }
        }

    }

    private fun observeLogInEvent() {
        viewModel.onLoggedInEvent.observe(this, Observer { resource ->
            run {
                resource?.let {
                    if (it.status == Resource.Status.SUCCESS) {
                        SnackbarBuilder.buildSnack(view!!, "Logged in.Starting sync").show()
                        val activity = requireActivity()
                        MainActivity.start(activity)
                        activity.finish()
                    } else if (it.status == Resource.Status.ERROR) {
                        SnackbarBuilder.buildErrorSnack(view!!, it.message?:"").show()
                    }
                }
            }
        })
    }
}