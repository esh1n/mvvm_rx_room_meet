package com.dataart.dartcard.presentation.di.fragments


import com.dataart.dameet.presentation.di.module.ViewModelsModule
import com.dataart.dameet.presentation.main.MainFragment


import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface CardsActivityModule {

    @ContributesAndroidInjector(modules = [ViewModelsModule::class])
    fun mainFragment(): MainFragment

}

