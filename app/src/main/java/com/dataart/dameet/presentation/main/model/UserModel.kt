package com.dataart.dameet.presentation.main.model


data class UserModel(val id: String,
                     val name: String,
                     val owner: Boolean = false)