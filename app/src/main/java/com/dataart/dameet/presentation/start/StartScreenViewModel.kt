package com.dataart.dameet.presentation.start

import com.esh1n.core_android.rx.SchedulersFacade
import com.dataart.dameet.domain.auth.AuthRepository
import com.esh1n.core_android.ui.viewmodel.BaseViewModel
import com.esh1n.core_android.ui.viewmodel.SingleLiveEvent

class StartScreenViewModel(private val authRepository: AuthRepository) : BaseViewModel() {

    val isUserAuthorized = SingleLiveEvent<Boolean>()

    fun chooseNavigation() {
        addDisposable(
            authRepository.isAuthorized
                .compose(SchedulersFacade.applySchedulersSingle())
                .subscribe() { isHasAuth -> isUserAuthorized.setValue(isHasAuth) }
        )
    }
}