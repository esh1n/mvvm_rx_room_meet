package com.dataart.dameet.presentation.main.adapter

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.dataart.dameet.R
import com.dataart.dameet.databinding.ItemUserBinding
import com.dataart.dameet.presentation.main.model.UserModel
import com.esh1n.utils_android.inflate


class UsersAdapter(private val clickHandler: (String) -> Unit) : RecyclerView.Adapter<UsersAdapter.ViewHolder>() {
    private var userModels: List<UserModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.context.inflate(R.layout.item_user, parent)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userModels?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cardModel = userModels?.get(position)
        if (cardModel != null) {
            holder.bindTo(cardModel)
        } else {
            holder.clear()
        }
    }


    fun swapCards(cardModels: List<UserModel>) {
        if (this.userModels == null) {
            this.userModels = cardModels
            notifyDataSetChanged()
        } else {
            val result = DiffUtil.calculateDiff(DiffCallback(this.userModels!!, cardModels))
            this.userModels = cardModels
            result.dispatchUpdatesTo(this)
        }
    }

    private inner class DiffCallback(private val oldCards: List<UserModel>, private val newCards: List<UserModel>) :
        DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldCards.size
        }

        override fun getNewListSize(): Int {
            return newCards.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val (id) = oldCards[oldItemPosition]
            val (id1) = newCards[newItemPosition]
            return id == id1
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldBrand = oldCards[oldItemPosition]
            val newBrand = newCards[newItemPosition]
            return oldBrand == newBrand
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val binding: ItemUserBinding?

        init {
            binding = DataBindingUtil.bind(itemView)

            //binding!!.imvFavoriteCard.setOnClickListener(this)
            itemView.setOnClickListener(this)
        }

        fun bindTo(cardModel: UserModel) {
            binding?.user = cardModel

            Log.d("image", "bind  to " + cardModel.id)
        }

        fun clear() {
            binding?.user = null
        }

        override fun onClick(v: View) {
            val userModel = userModels?.get(adapterPosition)
            userModel?.let {
                clickHandler.invoke(userModel.name)
            }

        }
    }

}