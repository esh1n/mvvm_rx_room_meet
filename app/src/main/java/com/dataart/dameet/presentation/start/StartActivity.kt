package com.dataart.dameet.presentation.start

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.dataart.dameet.R
import com.dataart.dameet.presentation.auth.AuthActivity
import com.dataart.dameet.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dameet.presentation.main.MainActivity
import com.esh1n.core_android.ui.activity.BaseDIActivity
import javax.inject.Inject


class StartActivity : BaseDIActivity() {
    @Inject
    internal lateinit var factory: AuthViewModelFactory

    private lateinit var viewModel: StartScreenViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        viewModel = ViewModelProviders.of(this, factory).get(StartScreenViewModel::class.java)
        observeNavigateEvents()
        viewModel.chooseNavigation()
    }

    private fun observeNavigateEvents() {
        viewModel.isUserAuthorized.observe(this, Observer { isAuthorized ->
            isAuthorized?.let {
                if (it) {
                    MainActivity.start(this)
                } else {
                    AuthActivity.start(this)
                }
            }
        })
    }
}