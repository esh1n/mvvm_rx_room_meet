package com.dataart.dameet.presentation.main

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.dataart.dameet.domain.main.UserRepository

class MainViewModelFactory(private val usersRepository: UserRepository) : ViewModelProvider.Factory {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(usersRepository) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
