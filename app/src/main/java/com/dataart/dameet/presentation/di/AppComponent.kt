package com.dataart.dameet.presentation.di

import com.dataart.dameet.App
import com.dataart.dameet.presentation.di.module.ActivitiesModule
import com.dataart.dameet.presentation.di.module.AppModule
import com.dataart.dameet.presentation.di.module.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, DataModule::class, ActivitiesModule::class])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }
}
