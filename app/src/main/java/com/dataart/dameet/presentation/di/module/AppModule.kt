package com.dataart.dameet.presentation.di.module

import android.content.Context
import android.content.SharedPreferences
import com.dataart.dameet.App
import com.dataart.dameet.data.local.cache.AppCacheContext
import com.dataart.dameet.data.local.room.AppDatabase
import com.esh1n.core_android.cache.CacheContext
import com.esh1n.core_android.cache.SharePreferenceSingleValueCache
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    internal fun provideDatabase(application: App): AppDatabase {
        return AppDatabase.getInstance(application.applicationContext)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(application: App): SharedPreferences {
        return application.getSharedPreferences("dataart.dartcard", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideCacheContext(sharedPreferences: SharedPreferences): AppCacheContext {
        val fileSystemCache = SharePreferenceSingleValueCache(sharedPreferences)
        return AppCacheContext(fileSystemCache)
    }

}