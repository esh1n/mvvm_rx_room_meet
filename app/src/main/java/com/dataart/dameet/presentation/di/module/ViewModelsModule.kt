package com.dataart.dameet.presentation.di.module

import com.dataart.dameet.data.local.cache.AppCacheContext
import com.esh1n.core_android.cache.CacheContext
import com.dataart.dameet.data.local.room.AppDatabase
import com.dataart.dameet.domain.auth.AuthRepository
import com.dataart.dameet.domain.main.UserRepository
import com.dataart.dameet.presentation.auth.viewmodel.AuthViewModelFactory
import com.dataart.dameet.presentation.main.MainViewModelFactory
import com.dataart.dameet.data.network.AuthorizationApiService
import com.dataart.dartcard.data.UserSessionApiService
import dagger.Module
import dagger.Provides


@Module
class ViewModelsModule {

    @Provides
    internal fun provideCardsViewModelFactory(database: AppDatabase, userSessionApiService: UserSessionApiService, cacheContext: AppCacheContext): MainViewModelFactory {
        val userRepository = UserRepository(userSessionApiService, database)
        return MainViewModelFactory(userRepository)
    }

    @Provides
    internal fun provideAuthViewModelFactory(database: AppDatabase, cacheContext: AppCacheContext, authorizationApiService: AuthorizationApiService): AuthViewModelFactory {
        val authRepository = AuthRepository(database, cacheContext, authorizationApiService)
        return AuthViewModelFactory(authRepository)
    }

}