package com.dataart.dameet.presentation.main

import android.arch.lifecycle.Observer
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.dataart.dameet.R
import com.dataart.dameet.databinding.FragmentMainBinding
import com.dataart.dameet.presentation.main.adapter.UsersAdapter
import com.esh1n.core_android.ui.fragment.BaseVMFragment
import com.esh1n.core_android.ui.viewmodel.Resource
import com.esh1n.utils_android.SnackbarBuilder
import com.esh1n.utils_android.setVisibleOrGone
import javax.inject.Inject

class MainFragment : BaseVMFragment<MainViewModel>() {

    @Inject
    override lateinit var factory: MainViewModelFactory

    override val viewModelClass = MainViewModel::class.java

    override val layoutResource = R.layout.fragment_main

    private var binding: FragmentMainBinding? = null

    private val userClickHandler: (String) -> Unit = {
        SnackbarBuilder.buildSnack(view!!, it).show()
    }

    private lateinit var adapter: UsersAdapter

    override fun setupView(rootView: View) {
        binding = DataBindingUtil.bind(rootView)
        binding?.let {
            val linearLayoutManager = LinearLayoutManager(activity)
            val dividerItemDecoration = DividerItemDecoration(requireActivity(), linearLayoutManager.orientation)
            adapter = UsersAdapter(userClickHandler)
            with(it.recyclerviewUsers) {
                addItemDecoration(dividerItemDecoration)
                layoutManager = linearLayoutManager
                setHasFixedSize(true)
                this.adapter = this@MainFragment.adapter
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeUsers()
        viewModel.users
    }

    private fun observeUsers() {
        viewModel.loadUsers()
        viewModel.users.observe(this, Observer { resource ->
            resource?.let {
                if (resource.status == Resource.Status.SUCCESS) {
                    val isEmpty = resource.data?.isEmpty() ?: true
                    binding?.tvEmptyUsers?.setVisibleOrGone(isEmpty)
                    adapter.swapCards(it.data!!)
                    if (isEmpty) {
                        SnackbarBuilder.buildSnack(view!!, "No users in brand").show()
                        requireActivity().supportFragmentManager.popBackStack()
                    }
                } else if (resource.status == Resource.Status.ERROR) {
                    SnackbarBuilder.buildErrorSnack(view!!, resource.message ?: "").show()
                }

            }

        })
    }
}