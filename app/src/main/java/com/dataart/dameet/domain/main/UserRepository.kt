package com.dataart.dameet.domain.main

import com.dataart.dameet.data.local.room.AppDatabase
import com.dataart.dameet.data.local.room.dao.UserDao
import com.dataart.dameet.data.local.room.entity.UserEntry
import com.dataart.dameet.domain.main.mapper.UserResponseMapper
import com.dataart.dartcard.data.UserSessionApiService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class UserRepository @Inject
constructor(private val userSessionApiService: UserSessionApiService, database: AppDatabase) {

    private val userDao: UserDao = database.userDao()

    fun loadUsers(): Observable<List<UserEntry>> {
        val users = userDao.users().toObservable()
        updateUsers()
        return users
    }

    private fun updateUsers() {
        userSessionApiService
            .users
            .map { UserResponseMapper().map(it) }
            .flatMapCompletable { users -> Completable.fromAction { insertUsers(users) } }
            .subscribeOn(Schedulers.io())
            .doOnError { error -> error.printStackTrace() }
            .subscribe()
    }

    private fun insertUsers(users:List<UserEntry>){
        userDao.insertOrUpdateUsers(users)
    }
}