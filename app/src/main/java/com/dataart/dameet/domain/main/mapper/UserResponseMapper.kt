package com.dataart.dameet.domain.main.mapper

import com.dataart.dameet.data.local.room.entity.UserEntry
import com.dataart.dameet.data.network.response.UserResponse
import com.esh1n.utils_android.Mapper

class UserResponseMapper : Mapper<UserResponse, UserEntry>() {
    override fun map(source: UserResponse): UserEntry {
        return UserEntry(id = source.id, name = source.name, owner = false)
    }
}