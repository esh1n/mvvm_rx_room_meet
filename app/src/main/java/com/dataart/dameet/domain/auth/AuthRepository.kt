package com.dataart.dameet.domain.auth

import com.dataart.dameet.data.local.cache.AppCacheContext
import com.dataart.dameet.data.local.room.AppDatabase
import com.dataart.dameet.data.network.AuthorizationApiService
import io.reactivex.Completable
import io.reactivex.Single

class AuthRepository(private val database: AppDatabase, private val cacheContext: AppCacheContext, private val authorizationApiService: AuthorizationApiService) {

    val isAuthorized: Single<Boolean>
        get() { return Single.just(!cacheContext.currentSessionToken.isNullOrBlank()) }

    fun login(login: String, pass: String): Completable {
        return Completable.fromAction { ->
            if (login != cacheContext.login) {
                cacheContext.clearSession()
                database.clearAllTables()
                cacheContext.login = login
            }
        }.andThen(authorizationApiService
            .getToken(login,pass))
            .map { it.token }
            .flatMapCompletable { token ->
                Completable.fromAction {
                    cacheContext.currentSessionToken = token
                }
            }

    }

    fun logout(): Completable {
        return Completable.fromAction {
            cacheContext.clearToken()
        }
    }
}
