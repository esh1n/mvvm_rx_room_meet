package com.dataart.dameet.data.local.room.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntry(@PrimaryKey val id: String,
                     val name: String,
                     val owner: Boolean = false)