package com.dataart.dameet.data.local.room.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import com.dataart.dameet.data.local.room.entity.UserEntry
import io.reactivex.Flowable
import io.reactivex.Observable

@Dao
abstract class UserDao {

    @Query("SELECT DISTINCT * FROM user")
    abstract fun user(): LiveData<UserEntry>

    @Query("SELECT * FROM user")
    abstract fun users(): Flowable<List<UserEntry>>

    @Query("SELECT DISTINCT id FROM user WHERE owner = 1")
    abstract fun ownerId(): String?

    @Insert(onConflict = IGNORE)
    abstract fun insert(user: UserEntry)


    @Update(onConflict = REPLACE)
    abstract fun update(user: UserEntry): Int

    @Transaction
    open fun insertOrUpdateUsers(users: List<UserEntry>) {
        for (user in users) {
            val updated = update(user)
            if (updated <= 0) {
                insert(user)
            }
        }
    }
}