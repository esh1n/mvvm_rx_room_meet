package com.dataart.dartcard.data


import com.dataart.dameet.data.network.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET

interface UserSessionApiService {

    @get:GET("/someapi/api/users")
    val users: Single<List<UserResponse>>

}

