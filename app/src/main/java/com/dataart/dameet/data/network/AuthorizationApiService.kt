package com.dataart.dameet.data.network

import com.dataart.dameet.data.network.response.TokenResponse

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthorizationApiService {

    @GET("/disco/google/token")
    fun getToken(@Query("authCode") login: String,password:String): Single<TokenResponse>
}
