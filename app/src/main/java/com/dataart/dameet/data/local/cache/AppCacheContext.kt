package com.dataart.dameet.data.local.cache

import com.esh1n.core_android.cache.CacheContext
import com.esh1n.core_android.cache.SingleValueCache

class AppCacheContext(fileSystemSingleValueCache: SingleValueCache) : CacheContext(fileSystemSingleValueCache) {
    private val tokenLock = Any()

    var currentSessionToken: String?
        get() {
            var token: String?
            synchronized(tokenLock) {
                token = prefsSecureStorage()?.getValue(SingleValueCache.KEY_TOKEN, String::class.java)
            }
            return token
        }
        set(newToken) {
            synchronized(tokenLock) {
                newToken?.let {
                    prefsSecureStorage()?.saveValue(SingleValueCache.KEY_TOKEN, newToken)
                }
            }
        }

    var login: String
        get() { return prefsSecureStorage()?.getValue(SingleValueCache.USER_EMAIL, String::class.java).orEmpty() }
        set(userEmail) {
            prefsSecureStorage()?.saveValue(SingleValueCache.USER_EMAIL, userEmail)
        }

    fun clearToken() {
        currentSessionToken = ""
    }

}