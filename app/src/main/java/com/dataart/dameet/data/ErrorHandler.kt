package com.dataart.dameet.data

import android.content.res.Resources
import com.dataart.dameet.BuildConfig


import retrofit2.Response

import java.net.HttpURLConnection.HTTP_BAD_REQUEST
import java.net.HttpURLConnection.HTTP_FORBIDDEN
import java.net.HttpURLConnection.HTTP_INTERNAL_ERROR
import java.net.HttpURLConnection.HTTP_NOT_FOUND
import java.net.HttpURLConnection.HTTP_UNAVAILABLE

class ErrorsHandler(private val mResources: Resources) {

//    @JvmOverloads
//    fun handle(error: Throwable, login: Boolean = false): String {
//        if (BuildConfig.ENABLE_LOGS) {
//            error.printStackTrace()
//        }

//        var message = mResources.getString(R.string.error_common)
//
//        if (error is RetrofitException) {
//            val response = (error as RetrofitException).getResponse()
//            if (response == null) {
//                message = mResources.getString(R.string.error_connection_state)
//            } else {
//                message = mResources.getString(getMessageResource(response!!, login))
//            }
//        }
//
//        return message
//    }
//
//    private fun getMessageResource(response: Response<*>, login: Boolean): Int {
//        when (response.code()) {
//            HTTP_BAD_REQUEST -> return if (login) R.string.error_invalid_login else R.string.error_server
//            HTTP_FORBIDDEN -> return R.string.error_access_denied
//            HTTP_NOT_FOUND -> return R.string.error_server
//            HTTP_INTERNAL_ERROR -> return R.string.error_server
//            HTTP_UNAVAILABLE -> return R.string.error_server
//            else -> return R.string.error_common
//        }
//    }
}