package com.dataart.dameet.data.local.room

import android.arch.persistence.room.*
import android.content.Context
import com.dataart.dameet.data.local.room.dao.UserDao
import com.dataart.dameet.data.local.room.entity.UserEntry
import java.util.*

@Database(entities = [UserEntry::class], version = 1, exportSchema = false)
@TypeConverters(AppConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {

        private const val DATABASE_NAME = "shareCard"

        fun getInstance(context: Context): AppDatabase {
            return Room.databaseBuilder(context.applicationContext,
                AppDatabase::class.java, AppDatabase.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
object AppConverters {
    @TypeConverter
    fun toDate(timestamp: Long?): Date? {
        return if (timestamp == null) null else Date(timestamp)
    }

    @TypeConverter
    fun toTimestamp(date: Date?): Long? {
        return date?.time
    }

}