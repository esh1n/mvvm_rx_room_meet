package com.dataart.dameet.data.network.response

import com.google.gson.annotations.SerializedName

data class UserResponse(@SerializedName("id") val id: String,
                        @SerializedName("name") val name: String)