package com.dataart.dameet.data.network.response

import com.google.gson.annotations.SerializedName

data class TokenResponse(@SerializedName("accessToken") val token: String)