package com.dataart.dameet

import android.app.Activity
import android.support.multidex.MultiDexApplication
import com.dataart.dameet.presentation.di.AppComponent
import com.dataart.dameet.presentation.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : MultiDexApplication(), HasActivityInjector {

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    lateinit var appComponent: AppComponent
        private set

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
    }

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>
}

